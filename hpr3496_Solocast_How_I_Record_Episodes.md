# Solocast - How I Record Episodes  [HPR 3496](http://hackerpublicradio.org/eps.php?id=3496)

I have been listening to HPR since the beginning.
I cant say I have listened to every episode, because I do skip a few.
But I have listened to most of the HPR episodes.

One of my favorite corespondents was Mr Gadgets
If you want to look him up, he was Host ID 155
If you haven't been listening to HPR for long,
you should go back and find some of Mr Gadget's episodes.
You will consider it time well spent.

Besides interesting topics, Mr Gadgets was a good story teller.
Even though His episodes were unedited, 
they still moved the listener along 
without interruption.

I cant do that.
When I speak off the cuff, I end up using a lot of filler words,
and I have to pause frequently to think about what I will say next.
I cant just start talking and a HPR episode falls out.

The first few episodes I made, I tried just talking into a recorder.
I kept stumbling over what I was trying to say 
and repeating myself multiple times to get out what I was trying to say.

I ended up with about an hour of audio
that I edited down to about a 15 minute HPR episode.
It took me a couple hours in audacity 
removing all the repeated sentences and filler words like UM and you know.

After I made a few episodes using the method where I would just
talk for a long time and create a giant recording that has to be edited
I thought it would be easier to record in small chunks
so I could stop the recording and think about what I wanted to say
between chunks of audio

I looked for a way to record short audio segments
and stitch them together later.
I know this is possible with audio editor.s
I tried recording into audacity 
stopping the recording to review my notes 
and think about what I wanted to say next

Audacity worked, but I had a hard time working with it.
its not you, its me

I started thinking about what would be ideal tool for me
to record HPR episodes.
I decided what I wanted was a tool that would
- Load a script from a text file
- Present the script in short segments
- Record the corresponding audio for the segment
- once all the segments were recorded, Combine the audio segments
- Post processing noise reduction and truncate silence

I looked for a tool that was designed to record audio
in short segments while also presenting a script
but I could not find anything.
It didn't seem too hard to implement
so I took a stab at writing some python.

I will have a copy of the script `solocast.py`
I will  go over how to use the script,
and then talk a bit about some of the script code.
The dependencies are pretty minimal.
The script uses `sox` to record and process the audio.
and the python click module to process the command line arguments

### Script
when solocast starts, it looks for and loads a file names script.txt
solocast breaks the script into segments.
The segments are split at empty lines.

The segments only have to contain as much information as you need.
I like to have *most* of the words I plan to say written in the script.
You may prefer only having bullet points, or even just the topic.

I like to keep the segments short.
That was the whole point of the project, to record several short segments.
For me, 1-2 minutes of audio is a good goal for segment length.

For the last few episodes I recorded,
I wrote the solocast script in markdown,
so the script could double as show notes. 

### Commands
To use the script, run `./solocast.py` followed by a command.
```
Commands:
  combine  Combine Segments into single audio file
  record   Record next unrecorded segment
  review   Print segments
  silence  Generate noise profile
```

The silence option records 5 seconds of audio used to generate a noise profile.
When I first started working in this project,
I was using a bad USB headset as a microphone
and therefor there was a lot of line noise in the recording.
I figured out that `sox` could replicate my normal process
of using audacity to remove noise.

The noise removal in `sox` works by first generating a noise profile
for a bit of silent audio
sox can then use the profile to subtract the noise from the audio.
the sox noise removal process worked surprisingly well
and is something I would consider a **Killer Feature** of `sox`

I wrote solocast.py to enforce the use of the noise profile.
the noise profile can be generated manually by running solocast silence
If you try to record audio and a noise profile does not already exist
solocast will jump to the function in the code to record the profile

### Review the script
running solocast review will present the script segments like when recording
but without recording audio,
This allows you to rehearse the script without recording any audio
This step is not required but hopefully useful

### Record
solocast uses the first 40 characters of the content of the script to determine the segment names
When you run solocast with the record option,
the script looks for the first segment that does not have an associated recording.
the text of the segment is printed, and
you will be prompted to press enter to start recording.

The script launches the `rec` command and records audio from the default sound input.
Press CRTL-c hen you are finished recording

Near the top of the script is an option to set the recording file format.
You can set it to anything that sox can recognize.  I just use wav.

```
(p)lay
(a)ccept
(r)eccord again
(t)runcate
```
when you are done, run solocast combine again to record the next segment.
The files are save in a directory named Recordings that is created when you first run solocast

### Combine
when you have recorded all the segments, the next step is to combine the recorded segments into one file.
Since solocast used the script to generate the segment recording file names
it can also use the script to put the segments in order.

solocast combine does the obvious step of combining the sediments,
but also applies the noise reduction using the silence profile.

the resulting file is `combined.wav`
The last few times I used solocast, 
I have opened the final combined file in audacity
to check the wav form for overall loudness 
and I scan the wav form for spikes that may be clicks that I want to edit out.
Otherwise, the combined file is ready for submission.

### What is next
- Gitlab
- rewrite with `pysox` library
- automate final review steps
  - loudness
  - loud clicks.

- pypi
- add optional recording tool, GUI??
- HPR Feedback episode
  - ffmpeg vs sox
  - apply noise profile prior toe submitting show

