# Install OpenBSD from Grub  [HPR 2181](http://hackerpublicradio.org/eps.php?id=2181) 

#### Install OpenBSD from Linux using Grub

### Why OpenBSD
Tune in for another episode.

### Why install from Linux
* Most VPS providers have images for Linux, but not OpenBSD
* Easier than trying to upload custom image or iso.

### Grub2
* Start with a distro that uses grub2.  I use Centos7
* grub2 can load OpenBSD kernels.
* The OpenBSD installer is a OpenBSD kernel.


### Procure
1. Make sure you have console access to the Linux VM
1. Record the Network info for the running Linux VM.  If not using DHCP, you will need to know the IP, netmask, default route (gateway), and a DNS server.
1. Download the OpenBSD installation ram disk to `/boot`
		
		cd /boot
		wget http://ftp5.usa.openbsd.org/pub/OpenBSD/6.0/amd64/bsd.rd

1. Reboot
1. Enter the grub command prompt by pressing `c` at the grub menu
1. The grub2 prompt has tab completion which can be helpful.
1. type `ls` to see the available disks
1. load the OpenBSD installation ram disk and boot

		grub> set root=(hd0,msdos1)
		grub> kopenbsd /bsd.rd
		grub> boot
### The Installation


* The Installer will ask you several questions
* The default is almost always what you want.  If unsure, just press enter.
* Look at the [FAQ](https://www.openbsd.org/faq/faq4.html) if you get stuck
* enter the network settings of the linux VPS
* When asked "Location of sets", use HTTP
