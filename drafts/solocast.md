### Why

The first few episodes I recorded,
I started with an outline of what I wanted to talk about.
The just recorded myself going over what I outlined about the topic.

Like most, I was very self critical when I listened back to the recording.
Besides the usual not liking the sound of my own voice,
I really did not like how often I used filler sounds.
I would say UMM, clear my throat, or click my teeth.

While I was recording, I would make a mistake.
I would sometimes record the same sentence multiple times.

I ended up spending more time editing that recording,
as much as an hour editing a 20 minute recording.

I recorded 3 episodes like this, outline, record, edit.

### I try editing in real time.
After not recording any new episodes for a few years,
I had a few topics that I wanted to talk about.
But I kept putting off recording because I did not want to do the editing.

I thought I could do better recording if I could edit as I was recording.
I've heard other podcasters say they do something similar.

So I started with audacity.
I would hit record, and stop when I made a mistake.

After fixing the mistake, I would start the recording again.

This process worked.  I made at least one episode using this method.
But us was clunky, and I was afraid if I stopped audacity 
I would loose all my work.
The process worked, but it was cumbersome.

### Idea

