set -euo pipefail
IFS=$'\n\t'

mkdir -pv public
for MD in $(ls *md)
	do
	echo
	# echo "---"
	# echo
	# echo "#" $MD
	echo
	echo "---"
	echo
	cat $MD


done    \
|pandoc \
-H markdown.header \
-B body.header \
--toc \
--toc-depth=1 \
-f gfm \
-t html \
-o public/index.html  
