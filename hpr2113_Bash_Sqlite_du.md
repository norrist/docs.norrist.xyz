
# Bash and Sqlite  [HPR 2113](http://hackerpublicradio.org/eps.php?id=2113) 

I need to monitor some directories and track their Weekly growth.
The process I came up with is:

* cron job  records the result of the `du` command to a file
* bash script to creates SQL
* sqlite
	* runs in memory
	* create tables for old and new `du` files
	* sets the field separator to tab
	* imports the `du` files into tables
	* sets the output mode to csv and output file to `deltas.csv`
	* turn on headers for the csv file
*  the SQL  query 
	* joins the tables based on the file path
	* calculates the growth
	* sorts result by growth
	* only shows directories that have grown
* the mailx command emails me the resulting csv	


### Crontab

`0 3 * * 0 /bin/du -m /data/ > /home/USER/du_files/"du_$(/bin/date +\%Y\%m\%d)"`

### Script


```	sh
cd ~/du_files
TODAYS_FILE="du_$(/usr/bin/date +%Y%m%d)"
YESTERDAYS_FILE="du_$(/usr/bin/date --date="7 days ago" +%Y%m%d)"
/usr/bin/echo "create table old (oldsize interger, path varchar);" > delta.sql
/usr/bin/echo "create table new (newsize interger, path varchar);" >> delta.sql
/usr/bin/echo '.separator "\t" ' >> delta.sql
/usr/bin/echo ".import $TODAYS_FILE new" >> delta.sql
/usr/bin/echo ".import $YESTERDAYS_FILE old" >> delta.sql
/usr/bin/echo ".mode csv" >> delta.sql
/usr/bin/echo ".headers on" >> delta.sql
/usr/bin/echo ".out deltas.csv" >> delta.sql
/usr/bin/echo "select *,newsize-oldsize as delta_in_megabytes from old natural join new where oldsize<newsize order by delta_in_megabytes desc;" >> delta.sql

/usr/bin/sqlite3 < delta.sql

echo $YESTERDAYS_FILE|/usr/bin/mailx -a deltas.csv -s deltas.csv me@mywork.com
```

Resulting SQL

``` sqlite
create table old (oldsize interger, path varchar);
create table new (newsize interger, path varchar);
.separator "\t"
.import du_20160821 new
.import du_20160814 old
.mode csv
.headers on
.out deltas.csv
select *,newsize-oldsize as delta_in_megabytes
from old 	natural join new 	where oldsize<newsize
order by delta_in_megabytes desc;
```